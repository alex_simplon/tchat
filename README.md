## les fichiers php

Arboressence des fichiers :

- Classes

  - Classe Utilisateurs
  - Classe Messages

        - Repositorry
            - UtilisateursRepository.php
            - MessagesRepository.php

- css

  - login.css
  - style.css

- mcd

  - mcdtch.mcd

- public
  - index.php

---

## Evolution du code

J'ai l'impression d'avoir crée une sucession de fichier php sans lien réel entre  
les uns et les autres qui ne mènent a aucune fonctionnalité!

## fonctionnalités opérationnelle

Aucune.  
Pas de fichier de connexion.  
Pas d'utilisatuers ni de messages qui s'ajout à la base de donnée.

## Conclusion

J'ai l'impression de ne pas évoluer. D'ête sous une cascade de code qui mène nul par.

---

# Interface de Tchat 1

## Créez une interface de chat pour communiquer à distance avec votre groupe de travail en temps réel !

---

## Contexte du projet

A cause des grèves de trains, vous ne pouvez pas rejoindre votre équipe pour travail en présentiel.  
Vous devez pourtant échanger avec eux... Créez un chat pour pouvoir échanger par message avec eux !  
Chaque personne doit s'authentifier pour chatter. Le pseudo doit être unique. Le mot de passe sera  
chiffré. Chaque message reçu s'affiche à gauche, quand les messages envoyés s'affichent à droite.

Ce tchat enregistre les messages en Base de données, pour ne pas les perdre lors de la déconnexion  
ou le rafraichissement de la page.

On veut que lorsqu'un nouveau message est posté, tout le monde dans la conversation le voit tout de suite.  
Il faudra trouver un moyen de mettre à jour le tchat, afin que les derniers messages s'affichent sans action de la part de l'utilisateur.

Si on a pas vu ses messages depuis longtemps, on veut voir apparaitre une pastille rouge avec le nombre de  
messages non-lus. On pourra également faire apparaitre ce nombre dans le nom de l'onglet, en clignotant.

On pourra enfin rendre repliable ce tchat, pour le garder actif mais discret. A terme, vous pourrez ainsi  
l'intégrer dans des projets d'interface client par exemple.

---

## Modalités pédagogiques

Travail individuel. Rendu le mardi 28/02 en début d'après-midi (7 après-midis).

---

## Livrables

Un dépôt git, avec le code, et un MCD.

---
