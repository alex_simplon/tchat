<?php
  function chargerClasse ($Classes){
    if (file_exists('classes/'.$Classes . '.php')) {
      require 'classes/'.$Classes . '.php'; // On inclue la classe correspondante au paramètre passé
    } else if(file_exists('repository/'.$Classes . '.php')){
      require 'repository/'.$Classes . '.php';
    } else {
      exit("Le fichier $Classes.php n'existe ni dans classe ni dans repository.");
    }

  }

spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée

session_start();