#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateurs
#------------------------------------------------------------

CREATE TABLE Utilisateurs(
        id_utilisateurs Int  Auto_increment  NOT NULL ,
        Nom             Varchar (150) NOT NULL ,
        Prenom          Varchar (150) NOT NULL ,
        Email           Varchar (150) NOT NULL ,
        Pseudo          Varchar (150) NOT NULL ,
        Password        Varchar (150) NOT NULL
	,CONSTRAINT Utilisateurs_PK PRIMARY KEY (id_utilisateurs)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Messages
#------------------------------------------------------------

CREATE TABLE Messages(
        id_message      Int  Auto_increment  NOT NULL ,
        Texte           Varchar (150) NOT NULL ,
        Date            Date NOT NULL ,
        Heure           Datetime NOT NULL ,
        id_utilisateurs Int NOT NULL
	,CONSTRAINT Messages_PK PRIMARY KEY (id_message)

	,CONSTRAINT Messages_Utilisateurs_FK FOREIGN KEY (id_utilisateurs) REFERENCES Utilisateurs(id_utilisateurs)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: lire
#------------------------------------------------------------

CREATE TABLE lire(
        id_message      Int NOT NULL ,
        id_utilisateurs Int NOT NULL
	,CONSTRAINT lire_PK PRIMARY KEY (id_message,id_utilisateurs)

	,CONSTRAINT lire_Messages_FK FOREIGN KEY (id_message) REFERENCES Messages(id_message)
	,CONSTRAINT lire_Utilisateurs0_FK FOREIGN KEY (id_utilisateurs) REFERENCES Utilisateurs(id_utilisateurs)
)ENGINE=InnoDB;

