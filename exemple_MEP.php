<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src=""https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/style.css">
  <title>exemple MEP</title>
</head>


<body>
  <div id="conteneur" class="conteneur">
    <div id="messages" class="messages">
      <p class="M_date">18/02/2023</p>
      <p class="M_date">10h12</p>
      <div id="m12" class="M M_recu">
        <h3>Anne</h3>
        <p class="M_message">Regarde comme c'est beau !</p>
      </div>
      <p class="M_date">10h13</p>
      <div id="m13" class="M M_recu">
        <h3>Pierre</h3>
        <p class="M_message">Oui très ! Il est trop fort !</p>
      </div>
      <p class="M_date">10h16</p>
      <div id="m14" class="M M_envoye">
        <h3>Moi</h3>
        <p class="M_message">Naann mais on peut en faire autant !!</p>
      </div>
      <p class="M_date">10h17</p>
      <div id="m15" class="M M_recu">
        <h3>Anne</h3>
        <p class="M_message">Vas-y, on te regarde ! 😉</p>
      </div>
    </div>
    <div id="post" class="post">
      <form action="#" method="POST">
        <textarea name="message" id="message" cols="30" rows="5" placeholder="votre message" class="new_message"></textarea>
        <input type="submit" name="newPost" value="Envoyer" class="new_submit">
      </form>
    </div>
  </div>


  <script>
    setInterval('actualisation()',10);
    function actualisation(){

      $('#messages')
    }
    </script>
</body>
</html>
